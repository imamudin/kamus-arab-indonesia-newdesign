package com.ristekmuslim.kamusarabindo;


import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;


import com.ristekmuslim.kamusarabindo.config.GlobalConfig;
import com.ristekmuslim.kamusarabindo.fragment.DBLama;
import com.ristekmuslim.kamusarabindo.fragment.LisanulArob;
import com.ristekmuslim.kamusarabindo.fragment.LisanulArab;
import com.ristekmuslim.kamusarabindo.fragment.Munawwir;
import com.ristekmuslim.kamusarabindo.fragment.Quran;

public class MainActivity extends AppCompatActivity {

    LinearLayout ll_main;

    private TabLayout tabLayout;
    private ViewPager viewPager;

    //Fragments
    Munawwir fMunawwir;
    DBLama fDBLama;
    LisanulArob fLisanulArob;
    LisanulArab fLisanulArab;
    Quran fQuran;

    public SearchView searchView;
    String s_searchView ="";
    private char char_arab_terkecil = '\u0600', char_arab_terbesar ='\u06ff';
    private static String word="";

    SharedPreferences mypref;
    Boolean first_query = true;

    Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab_without_icon);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.app_name));
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setHomeButtonEnabled(false);

        init();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {


            @Override
            public boolean onQueryTextSubmit(String query) {
                //action ketika pilih tombol search
                searchWord(query);
                s_searchView = query;
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //action ketika kata ganti
//                searchWord(newText);
//                s_searchView = newText;
//
//                //menampilkan geser untuk pertama kali query
//                if(first_query){
//                    first_query = false;
//                    notifFirstQuery(getString(R.string.first_query));
//
//                    //menyimpan ke database first query
//                    SharedPreferences.Editor editor = mypref.edit();
//                    editor.putBoolean(GlobalConfig.PREF_FIRST_QUERY, false).commit();
//                }
                s_searchView = newText;
               // if(s_searchView.length()<=2) {
                    mHandler.removeCallbacksAndMessages(null);

                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Put your call to the server here (with mQueryString)
                            searchWord(s_searchView);
                        }
                    }, 300);
                //} else {
                //    searchWord(s_searchView);
               // }

                return false;
            }
        });


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                viewPager.setCurrentItem(position,false);
                //openSnackBar(getString(R.string.refresh));
                if (position==0) {
                    //Toast.makeText(getApplicationContext(),"Geser slide ke kiri atau kanan untuk pindah kamus", Toast.LENGTH_SHORT).show();
                    openSnackBar("Kamus Arab Indonesia - klik refresh");
                    searchView.setQueryHint("ketik kata عرب / indonesia");
                } else if (position==1) {
                    //Toast.makeText(getApplicationContext(),"Geser slide ke kiri atau kanan untuk pindah kamus", Toast.LENGTH_SHORT).show();
                    openSnackBar("Kamus Munawwir - klik refresh");
                    searchView.setQueryHint("ketik kata عرب / indonesia");
                } else if (position==2) {
                    //Toast.makeText(getApplicationContext(),"Geser slide ke kiri atau kanan untuk pindah kamus", Toast.LENGTH_SHORT).show();
                    openSnackBar("معجم العرب - klik refresh");
                    searchView.setQueryHint("ketik kata عرب");
                    word=searchView.getQuery().toString();
                    if (word.length()!=0) {
                        final boolean isArab = isArab(word.toLowerCase().charAt(0));
                        if (!isArab) {
                            //searchView.setQuery("",true);
                            Toast.makeText(getApplicationContext(), "gunakan font arab", Toast.LENGTH_SHORT).show();
                        }
                    }
                    //if(searchView.getQuery().charAt(0)>'\u0600' && searchView.getQuery()!=null){
                    //    searchView.setQuery("",true);
                    //    Toast.makeText(getApplicationContext(),"running on this", Toast.LENGTH_SHORT).show();
                    //}
                } else if (position==3) {
                    //Toast.makeText(getApplicationContext(),"Geser slide ke kiri atau kanan untuk pindah kamus", Toast.LENGTH_SHORT).show();
                    openSnackBar("لسان العرب - klik refresh");
                    searchView.setQueryHint("ketik kata عرب");
                    word=searchView.getQuery().toString();
                    if (word.length()!=0) {
                        final boolean isArab = isArab(word.toLowerCase().charAt(0));
                        if (!isArab) {
                            //searchView.setQuery("",true);
                            Toast.makeText(getApplicationContext(), "gunakan font arab", Toast.LENGTH_SHORT).show();
                        }
                    }
                    //if(searchView.getQuery().charAt(0)>'\u0600' && searchView.getQuery()!=null){
                    //    searchView.setQuery("",true);
                    //    Toast.makeText(getApplicationContext(),"running on this", Toast.LENGTH_SHORT).show();
                    //}
                } else if (position==4) {
                    //Toast.makeText(getApplicationContext(),"Geser slide ke kiri atau kanan untuk pindah kamus", Toast.LENGTH_SHORT).show();
                    openSnackBar("Kamus Al-Qur'an - klik refresh");
                    searchView.setQueryHint("ketik kata عرب / indonesia");
                    //if(searchView.getQuery().charAt(0)>'\u0600' && searchView.getQuery()!=null){
                    //    searchView.setQuery("",true);
                    //    Toast.makeText(getApplicationContext(),"running on this", Toast.LENGTH_SHORT).show();
                    //}
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        //setupViewPager viewPager;
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        fMunawwir =new Munawwir();
        fDBLama =new DBLama();
        fLisanulArob =new LisanulArob();
        fQuran =new Quran();
        fLisanulArab = new LisanulArab();
        adapter.addFragment(fDBLama,"KAMUS ARAB INDONESIA");
        adapter.addFragment(fMunawwir,"MUNAWWIR");
        adapter.addFragment(fLisanulArob,"MU'JAM ARAB");
        adapter.addFragment(fLisanulArab,"LISANUL ARAB");
        adapter.addFragment(fQuran,"AL-QUR'AN");
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);
        Toast.makeText(getApplicationContext(),"Geser slide layar hp ke kiri atau kanan untuk pindah kamus", Toast.LENGTH_SHORT).show();

    }
    private boolean isArab(char x){
        if(x > char_arab_terkecil){
            return  true;
        }
        return false;
    }
    private void searchWord(String newText){
        Fragment page = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewpager + ":" + viewPager.getCurrentItem());
        if (viewPager.getCurrentItem() == 0 && page != null) {  //untuk db lama
            ((DBLama)page).callDatas(newText);
        }else if (viewPager.getCurrentItem() == 1 && page != null) { //untuk munawir
            ((Munawwir)page).callDatas(newText);
        }else if (viewPager.getCurrentItem() == 2 && page != null) { //untuk mujam arab
            ((LisanulArob)page).callDatas(newText);
        }else if (viewPager.getCurrentItem() == 3 && page != null) { //untuk mujam arab
            ((LisanulArab)page).callDatas(newText);
        }else if (viewPager.getCurrentItem() == 4 && page != null) { //untuk quran
            ((Quran)page).callDatas(newText);
        }
    }
    private void init(){
        ll_main     = (LinearLayout)findViewById(R.id.ll_main);
        mypref      = getSharedPreferences(GlobalConfig.NAMA_PREF, MODE_PRIVATE);

        first_query = mypref.getBoolean(GlobalConfig.PREF_FIRST_QUERY,true);

        //Initializing viewPager
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(3);

        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(viewPager);

        searchView  = (SearchView) findViewById(R.id.searchView);

        mHandler =  new Handler();

    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        // Associate searchable configuration with the SearchView
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        int id = item.getItemId();
        switch (item.getItemId()) {
            case R.id.action_munawwir:
                viewPager.setCurrentItem(1);
                return true;
            case R.id.action_arabindo:
                viewPager.setCurrentItem(0);
                return true;
            case R.id.action_mujamarab:
                viewPager.setCurrentItem(2);
                return true;
            case R.id.action_lisanularab:
                viewPager.setCurrentItem(3);
                return true;
            case R.id.action_quran:
                viewPager.setCurrentItem(4);
                return true;
            case R.id.action_about:
                show_about();
                return true;
            case R.id.action_share:
                share();
                return true;
            case R.id.action_rate:
                rate();
                return true;
            case R.id.action_update:
                check_updates();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void openSnackBar(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_SHORT);
        snack.setActionTextColor(getResources().getColor(android.R.color.white ))
                .setActionTextColor(getResources().getColor(R.color.colorPrimary))
                .setAction("Refresh", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        searchWord(s_searchView);
                    }
                })
                .show();
    }
    private void notifFirstQuery(String message){
        Snackbar snack = Snackbar.make(ll_main, message, Snackbar.LENGTH_SHORT);
        snack.setActionTextColor(getResources().getColor(android.R.color.holo_green_light ))
                .setActionTextColor(getResources().getColor(R.color.colorPrimary))
                .show();
    }

    private void check_updates(){
        Intent url_app=new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id="+getPackageName()));
        startActivity(url_app);
    }

    private void show_about(){
        String title = ""+getResources().getString(R.string.app_name);
        String text = "<b>Silahkan baca informasi di bawah ini</b>, bila kesulitan menggunakan aplikasi silahkan hubungi kami via whatsapp : <a href=\"https://api.whatsapp.com/send?phone=6285231423786&text=Assalamu'alaikum, RistekMuslim\">085231423786</a><br>"+
                "<br>\n" +
                "Kamus Arab Indonesia adalah aplikasi kamus bahasa arab pertama di android yang dapat menterjemahkan kata dari bahasa Indonesia ke bahasa Arab dan juga sebaliknya. Aplikasi ini mulai dikembangkan pada 2012 dan masih terus dalam tahap pengembangan.<br> \n" +
                "<br>\n" +
                "<i>Aplikasi ini sangat cocok dan kami rekomendasikan untuk para pelajar dan pengajar bahasa arab</i>. <b>Semoga dengan adanya aplikasi ini, lebih menambah semangat kita dalam mencintai dan mempelajari bahasa Arab.</b> Saat ini ada beberapa fitur aplikasi yang sudah bisa anda manfaatkan :<br>"+
                "<br>\n" +
                "1. Menterjemahkan kata Indonesia - Arab<br>\n" +
                "2. Menterjemahkan kata Arab - Indonesia<br>\n" +
                "3. Database: 154.644 kosa kata <br>\n" +
                "4. Mu'jamul Arab (Arab - Arab): 29803 mufrodat <br>\n" +
                "5. Lisanul Arab (Arab - Arab): 9415 mufrodat <br>\n" +
                "6. Kamus Al-Qur'an dilengkapi mukhtashor fi tafsir, mudah dan cepat mencari ayat dalam alquran<br>\n" +
                "7. Profil Aplikasi<br>\n" +
                "8. Share Aplikasi<br>\n" +
                "<br>\n" +
                "<b>Catatan :</b> <br>\n" +
                "1. Untuk penulisan arab (keyboard arab), kami rekomendasikan menggunakan Gboard (Google Keyboard), silahkan masuk pada Setting > Input Bahasa > Keyboard pilih 'Gboard' > Bahasa, Lalu klik pilihan bahasa defaultnya masih English/Indonesia, pada list 'Bahasa Arab/ Arabic itu di aktifkan.<br>\n" +
                "<br>\n" +
                "2. Tab paling kiri untuk Kamus Arab Indonesia, munawwir, mu'jam arab, lisanul arab, dan tab paling kanan Kamus Al-Qur'an .<br>\n" +
                "<br>\n" +
                "3. Aplikasi kamus ini hanya menterjemahkan per kata. Jika menulis kalimat akan diterjemahkan perkata <br>\n" +
                "<br>\n" +
                "4. Untuk android versi 3 kebawah atau android yang belum support bahasa Arab, silahkan menginstall aplikasi arabic keyboard (kami rekomendasikan menginstall \"anysoftkeyboard\" + update pack arabic language-nya) terlebih dahulu untuk transliterasi kata Arab - Indonesia.<br>\n" +
                "<br>\n" +
                "Anda dapat memberikan ide,saran, atau koreksi aplikasi Kamus Arab Indonesia dengan menghubungi kami di :<br>" +
                "email : android@ristekmuslim.com<br>\n" +
                "whatsapp : <a href=\"https://api.whatsapp.com/send?phone=6285231423786&text=Assalamu'alaikum, Ristek Muslim\">085231423786</a><br>" +
                "website : <a href=\"http://ristekmuslim.com\">http://ristekmuslim.com</a> | <a href=\"http://ristekmuslim.id\">http://ristekmuslim.id</a><br>\n" +
                "facebook : <a href=\"https://www.facebook.com/RistekMuslim\">Ristek Muslim</a>\n" +
                "<br>\n" +
                "twitter : <a href=\"https://twitter.com/@ristekmuslim\">Ristek Muslim</a><br>\n" +
                "<br>\n" +
                "Mudah-mudahan upaya kami mengembangkan aplikasi ini mendapat pahala kebaikan dari Allah subahanu wata'ala, dan aplikasi ini bermanfaat untuk kaum muslimin.<br>\n" +
                "<br>\n" +
                "Bantu rate dan share aplikasi Kamus Arab Indonesia, agar semakin banyak yang mendapat manfaat dari aplikasi ini.<br>\n" +
                "<br><br>\n" +
                "<b>Tim Ristek Muslim</b>";
        LayoutInflater layoutInflater = (LayoutInflater)this.getLayoutInflater();
        View promptView = layoutInflater.inflate(R.layout.notif_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptView);

        final TextView t_title  = (TextView) promptView.findViewById(R.id.t_title_dialog);
        final TextView t_text   = (TextView) promptView.findViewById(R.id.t_text_dialog);

        t_title.setText(title);
        t_text.setText(Html.fromHtml(text));
        t_text.setMovementMethod(LinkMovementMethod.getInstance());
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setNegativeButton("Tutup",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }
    public void rate(){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id="+getPackageName())));
        }
    }
    public void share(){
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, "Ayo Install Aplikasi Kamus Arab Indonesia \nhttp://play.google.com/store/apps/details?id="+getPackageName());
        startActivity(Intent.createChooser(shareIntent, "Share...."));
    }

}
