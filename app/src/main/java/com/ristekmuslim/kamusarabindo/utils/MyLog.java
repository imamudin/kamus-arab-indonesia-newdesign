package com.ristekmuslim.kamusarabindo.utils;

import android.util.Log;

import com.ristekmuslim.kamusarabindo.config.GlobalConfig;

/**
 * Created by imamudin on 08/12/17.
 */

public class MyLog {
    String environment = GlobalConfig.ENVIRONMENT;
    String nameClass;

    public MyLog() {
    }
    public MyLog(String nameClass) {
        this.nameClass = nameClass;
    }
    public void print(String msg) {
        if(nameClass == null) nameClass  = getClass().toString();
        this.write(nameClass, msg);
    }
    public void print(String nameClass, String msg) {
        this.write(nameClass, msg);
    }
    public void printError(String msg) {
        Log.d(nameClass, msg);
    }
    private void write(String nameClass, String msg){
        if(environment.equals("development"))
            Log.d(nameClass, msg);
        else if (environment.equals("production"));

    }
}
