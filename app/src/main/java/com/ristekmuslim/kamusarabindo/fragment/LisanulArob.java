package com.ristekmuslim.kamusarabindo.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.ristekmuslim.kamusarabindo.DetailWord;
import com.ristekmuslim.kamusarabindo.R;
import com.ristekmuslim.kamusarabindo.adapter.CustomListAdapterLisanulArab;
import com.ristekmuslim.kamusarabindo.db.DatabaseAccess2;
import com.ristekmuslim.kamusarabindo.db.QueryData;

import java.util.ArrayList;

/**
 * Created by imamudin on 17/01/19.
 */

public class LisanulArob extends Fragment {
    private ArrayList data, wordsArab;
    private ListView list_query;
    private CustomListAdapterLisanulArab adapter_arab;

    private char char_arab_terkecil = '\u0600', char_arab_terbesar ='\u06ff';
    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.lisanul_arob, container, false);

        return rootView;
    }
    public void callDatas(String word){

        if (!word.equals("")) {
            DatabaseAccess2 databaseAccess2 = DatabaseAccess2.getInstance(getContext());
            databaseAccess2.open();

            final boolean isArab = isArab(word.toLowerCase().charAt(0));

            if(isArab){
                wordsArab = databaseAccess2.getWordsArab_lisanularob(word);
                list_query = (ListView)rootView.findViewById(R.id.lv_result);
                adapter_arab = new CustomListAdapterLisanulArab(getContext(), wordsArab);

                list_query.setAdapter(adapter_arab);

                registerForContextMenu(list_query);
                list_query.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                        Object o = list_query.getItemAtPosition(position);
                        QueryData newsData = (QueryData) o;

                        Intent myIntentA1A2 = new Intent(getContext(), DetailWord.class);
                        Bundle myData = new Bundle();

                        myData.putBoolean("isArab", isArab);
                        String sentence = newsData.getIndo();
                        sentence = sentence.replace("|", "\n");
                        myData.putString("arab", "" + sentence);
                        myData.putString("indo", newsData.getArab());

                        myIntentA1A2.putExtras(myData);

                        startActivityForResult(myIntentA1A2, 103);
                    }
                });
                adapter_arab.notifyDataSetChanged();
            }else{
                data = new ArrayList<QueryData>();

                QueryData one = new QueryData();
                one.setIndo("maaf kata '"+word+"' tidak ditemukan dalam kamus.\nsaran: gunakan font arab untuk mencari. Panduan keyboard Arab baca di menu About");
                data.add(one);

                list_query = (ListView)rootView.findViewById(R.id.lv_result);
                adapter_arab = new CustomListAdapterLisanulArab(getContext(), data);

                list_query.setAdapter(adapter_arab);
                registerForContextMenu(list_query);
                adapter_arab.notifyDataSetChanged();
            }
            databaseAccess2.close();
        } else {
            data = new ArrayList<QueryData>();
            list_query = (ListView)rootView.findViewById(R.id.lv_result);
            adapter_arab = new CustomListAdapterLisanulArab(getContext(), data);

            list_query.setAdapter(adapter_arab);
            registerForContextMenu(list_query);
            adapter_arab.notifyDataSetChanged();
        }
    }
    private boolean isArab(char x){
        if(x > char_arab_terkecil){
            return  true;
        }
        return false;
    }
}