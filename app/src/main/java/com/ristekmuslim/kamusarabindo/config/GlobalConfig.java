package com.ristekmuslim.kamusarabindo.config;

/**
 * Created by agung on 09/03/2016.
 */
public class GlobalConfig {
    public static final String ENVIRONMENT      = "development";   //production  -> log tidak muncul, development -> log muncul
    public static final String NAMA_PREF        = "com.muslimtekno.kamusarabindonesia.pref";  //key untuk menyimpan file preferences
    public static final String PREF_FIRST_QUERY = "first_query";  //key untuk menyimpan pertama kali search

    public static final String TAG              = "kai";                  //key untuk LOG


    /*
    PR yng belum dikerjakan

    * jangan selalu open database
    * mengganti tab fokus ditengah
    * menambah delay

    */
}
