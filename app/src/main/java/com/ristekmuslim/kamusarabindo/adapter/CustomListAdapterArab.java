package com.ristekmuslim.kamusarabindo.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ristekmuslim.kamusarabindo.R;
import com.ristekmuslim.kamusarabindo.db.QueryData;

import java.util.ArrayList;

/**
 * Created by agung on 03/09/2015.
 */
public class CustomListAdapterArab extends BaseAdapter {
    private ArrayList<QueryData> listData;
    private LayoutInflater layoutInflater;

    public CustomListAdapterArab(Context aContext, ArrayList<QueryData> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_row_layout_arab, null);
            holder = new ViewHolder();
            holder.arab = (TextView) convertView.findViewById(R.id.tv_indo);
            holder.indo = (TextView) convertView.findViewById(R.id.tv_arab);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (listData.get(position).getIndo()!=null) {
            holder.indo.setText(Html.fromHtml(listData.get(position).getIndo()));
        } else{
            holder.indo.setText(listData.get(position).getIndo());
        }
        holder.arab.setText(listData.get(position).getArab());
        return convertView;
    }

    static class ViewHolder {
        TextView arab;
        TextView indo;
    }
}
