package com.ristekmuslim.kamusarabindo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ristekmuslim.kamusarabindo.R;
import com.ristekmuslim.kamusarabindo.db.QueryData;

import java.util.ArrayList;

/**
 * Created by agung on 22/12/2015.
 */
public class CustomListAdapterLisanulArab extends BaseAdapter {
    private ArrayList<QueryData> listData;
    private LayoutInflater layoutInflater;

    public CustomListAdapterLisanulArab(Context aContext, ArrayList<QueryData> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_row_layout_lisanul_arob, null);
            holder = new ViewHolder();
            holder.arab = (TextView) convertView.findViewById(R.id.tv_arab);
            holder.indo = (TextView) convertView.findViewById(R.id.tv_indo);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String sentence = listData.get(position).getIndo();
        sentence = sentence.replace("|", "\n");
        sentence = sentence.replace("\\r\\n", "\n");

        holder.indo.setText(sentence);
        holder.arab.setText(listData.get(position).getArab());
        return convertView;
    }
    static class ViewHolder {
        TextView arab;
        TextView indo;
    }
}

