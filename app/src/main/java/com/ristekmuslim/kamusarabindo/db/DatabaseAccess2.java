package com.ristekmuslim.kamusarabindo.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

/**
 * Created by imamudin on 21/01/19.
 */

public class DatabaseAccess2 {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccess2 instance;

    private static String TABLE_ARAB        = "arab_indo";
    private static String TABLE_ARAB2       = "arab_indo2";
    private static String TABLE_ARAB3       = "quran";
    private static String COLUMN_ID         = "id";
    private static String COLUMN_ARAB       = "arab";
    private static String COLUMN_INDO       = "indonesia";
    private static String COLUMN_INDO_DASAR = "dasar";
    private static String COLUMN_ARAB_NOHAR = "noharokah";

    private static String TABLE_ARAB_ARAB           = "arabic_arabic";
    private static String TABLE_ARAB_ARAB2         = "lisanularab";
    private static String ARABIC_ID                 = "arabic_id";
    private static String ARABIC_WORD               = "arabic_word";
    private static String ARABIC_ROOT               = "arabic_root";
    private static String ARABIC_MEANINGS           = "arabic_meanings";
    private static String ARABIC_NOHAROKAH          = "arabic_noharokah";

    private static final String[] arabicUnicode = {
            "\u0618","\u0619","\u061a",
            "\u064b","\u064c","\u064d","\u064e","\u064f","\u0650","\u0651",
            "\u0652","\u0653","\u0654","\u0655","\u0656","\u0657","\u0658",
            "\u0659","\u065a","\u065b","\u065c","\u065d","\u065e","\u065f"
    };
    private DatabaseAccess2(Context context) {
        this.openHelper = new DatabaseOpenHelper(context);
    }
    public static DatabaseAccess2 getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess2(context);
        }
        return instance;
    }
    public void open() {
        this.database = openHelper.getWritableDatabase();
    }
    public void close() {
        if (database != null) {
            this.database.close();
        }
    }
    public void replace(){
        String replace = "";

        for(int i=0;i<arabicUnicode.length;i++){
            if(i==0)
                replace = "REPLACE("+COLUMN_ARAB +",'"+arabicUnicode[i]+"','')";
            else
                replace = "REPLACE("+replace +",'"+arabicUnicode[i]+"','')";
        }
        String selection = replace+" LIKE ?";

        Log.d("query",selection);
    }
    public static int is_tasdid(StringBuffer a){
        for(int i =0; i<a.length()-1;i++){
            char awal = a.charAt(i);
            if(awal == a.charAt(i + 1)){
                return i;
            }
        }
        return -1;
    }
    public static String hapus_harokah(String word){
        for(int i=0; i<arabicUnicode.length;i++){
            word = word.replace(arabicUnicode[i],"");
        }
        return  word;
    }
    public static boolean tasdid(StringBuffer a){
        for(int i =0; i<a.length();i++){
            if(a.charAt(i) == '\u0651'){
                return true;
            }
        }
        return false;
    }
    public ArrayList<QueryData> getWordsArab_Munawwir(String word){
        word = hapus_harokah(word);
        Log.i("query", word);
        char tasdid = '\u0651', alif='\u0627', alif_hamzah = '\u0623', alif_hamzah2 = '\u0625', alif_panjang='\u0622';
        StringBuffer word_sb = new StringBuffer(word);
        String s  = "";
        String s2 = "";
        String s3 = "";

        if (word.indexOf(alif)!=-1) {
            s = s+" or  "+COLUMN_ARAB_NOHAR+" like '%"+word.replace(alif,alif_hamzah)+"%' ";
            s = s+" or  "+COLUMN_ARAB_NOHAR+" like '%"+word.replace(alif,alif_hamzah2)+"%' ";
            s = s+" or  "+COLUMN_ARAB_NOHAR+" like '%"+word.replace(alif,alif_panjang)+"%' ";

            s2 = s2+" or  "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_hamzah)+" %' ";
            s2 = s2+" or  "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_hamzah2)+" %' ";
            s2 = s2+" or  "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_panjang)+" %' ";

            s3 = s3+" or "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_hamzah)+" - %' or "+COLUMN_ARAB_NOHAR+" like '% - "+word.replace(alif,alif_hamzah)+"' ";
            s3 = s3+" or "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_hamzah2)+" - %' or "+COLUMN_ARAB_NOHAR+" like '% - "+word.replace(alif,alif_hamzah2)+"' ";
            s3 = s3+" or "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_panjang)+" - %' or "+COLUMN_ARAB_NOHAR+" like '% - "+word.replace(alif,alif_panjang)+"' ";
        }

        ArrayList<QueryData> data = new ArrayList<QueryData>();
        QueryData one;
        String query="";
        String query2="";
        Integer counter=0;


        if (word.length()>2){
            query ="SELECT * FROM "+TABLE_ARAB+" where ("+COLUMN_ARAB_NOHAR+" like '"+word+"%' or "+COLUMN_ARAB_NOHAR+" like '%والـ"+word+"%' or "+COLUMN_ARAB_NOHAR+" like '%ال"+word+"%' or "+COLUMN_ARAB_NOHAR+" like '%"+word+"' or "+COLUMN_ARAB_NOHAR+" like '% "+word+" %' "+s+") ORDER BY "+COLUMN_ID+" ASC";
            query2 ="SELECT * FROM "+TABLE_ARAB+" where ("+COLUMN_ARAB_NOHAR+" like '"+word+"' or "+COLUMN_ARAB_NOHAR+" like '"+word+" - %' or "+COLUMN_ARAB_NOHAR+" like '% - "+word+"%' "+s3+") ORDER BY "+COLUMN_ID+" ASC";
        } else {
            query ="SELECT * FROM "+TABLE_ARAB+" where "+COLUMN_ARAB_NOHAR+" like '"+word+" %' or "+COLUMN_ARAB_NOHAR+" like '% "+word+" %' "+s2+"  ORDER BY "+COLUMN_ARAB_NOHAR+" ASC";
            query2 ="SELECT * FROM "+TABLE_ARAB+" where "+COLUMN_ARAB_NOHAR+" like '"+word+"'  ORDER BY "+COLUMN_ID+" ASC";
        }


        Cursor cursor=database.rawQuery("SELECT * FROM arab_indo where id=1", null);

        String[] words= word.split(" ");
        if (words.length > 1) {
            for (int i=0;i<words.length;i++) {
                s = "("+COLUMN_ARAB_NOHAR+" like '"+words[i]+"' or "+COLUMN_ARAB_NOHAR+" like 'ال"+words[i]+"' or "+COLUMN_ARAB_NOHAR+" like '"+words[i]+" - %' or "+COLUMN_ARAB_NOHAR+" like '% - "+words[i]+"')";
                s2 = "("+COLUMN_ARAB_NOHAR+" like '"+words[i]+" %' or "+COLUMN_ARAB_NOHAR+" like '% "+words[i]+"')";

                query ="SELECT * FROM "+TABLE_ARAB+" where "+s+" ORDER BY LENGTH("+COLUMN_ARAB_NOHAR+") ASC LIMIT 1";

                cursor = database.rawQuery(query, null);
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    one = new QueryData();
                    one.setArab(cursor.getString(2));
                    one.setIndo(cursor.getString(1));
                    data.add(one);
                    cursor.moveToNext();
                }
                Log.i("query", query);
            }
        } else {
            cursor = database.rawQuery(query2, null);
            cursor.moveToFirst();
            counter=cursor.getCount();
            ArrayList<String> tmp = new ArrayList<String>();
            while (!cursor.isAfterLast()) {
                one = new QueryData();
                tmp.add(cursor.getString(0));
                String tampung[] = (cursor.getString(5)).split(" ");
                String arabic[]  = (cursor.getString(1)).split(" ");
                Integer i;

                one.setIndo(cursor.getString(1));
                if (tampung.length <= arabic.length){
                    for (i=0;i<tampung.length;i++) {
                        if (tampung[i].contains(word)){
                            arabic[i]="<font color='#064B7F'><b>"+arabic[i]+"</b></font>";
                        }
                    }
                    String arabic_hasil="";
                    for(i=0;i<arabic.length;i++) {
                        arabic_hasil = arabic_hasil +" "+ arabic[i];
                    }

                    one.setIndo(arabic_hasil);
                }

                one.setArab(cursor.getString(2));
                //one.setIndo(cursor.getString(1));
                data.add(one);
                cursor.moveToNext();
            }
            if (word.length()>2){
                cursor = database.rawQuery(query, null);
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    if(!tmp.contains(cursor.getString(0))) {
                        one = new QueryData();
                        String tampung[] = (cursor.getString(5)).split(" ");
                        String arabic[]  = (cursor.getString(1)).split(" ");
                        Integer i;

                        one.setIndo(cursor.getString(1));
                        if (tampung.length <= arabic.length){
                            for (i=0;i<tampung.length;i++) {
                                if (tampung[i].contains(word)){
                                    arabic[i]="<font color='#064B7F'><b>"+arabic[i]+"</b></font>";
                                }
                            }
                            String arabic_hasil="";
                            for(i=0;i<arabic.length;i++) {
                                arabic_hasil = arabic_hasil +" "+ arabic[i];
                            }

                            one.setIndo(arabic_hasil);
                        }
                        one.setArab(cursor.getString(2));
                        //one.setIndo(cursor.getString(1));
                        data.add(one);
                    }
                    cursor.moveToNext();
                }
            }

            if (cursor.getCount()==0 && counter==0) {
                one = new QueryData();
                one.setArab("maaf kata '"+word+"' tidak ditemukan dalam kamus.\nsaran: silakan gunakan padanan kata yang lain");
                data.add(one);
            }
        }

        Log.i("query", query);
        Log.i("query2", query2);

        cursor.close();
        return data;
    }
    public ArrayList<QueryData> getWordsIndo_Munawwir(String word) {
        ArrayList<QueryData> data = new ArrayList<QueryData>();
        QueryData one;
        //Cursor cursor = database.rawQuery("SELECT * FROM "+TABLE_ARAB+" where "+COLUMN_INDO+" like '%"+word+"%' ORDER BY  LENGTH("+COLUMN_INDO+") ASC");
        //word="ka'bah";
        String query ="SELECT * FROM "+ TABLE_ARAB +" where ("+COLUMN_INDO+" like ? or "+COLUMN_INDO+" like ? or "+COLUMN_INDO+" like ? or "+COLUMN_INDO+" like ?) ORDER BY "+COLUMN_ID+" ASC";
        String query2 ="SELECT * FROM "+ TABLE_ARAB +" where ("+COLUMN_INDO+" like ? or "+COLUMN_INDO+" like ? or "+COLUMN_INDO+" like ? or "+COLUMN_INDO+" like ?) ORDER BY "+COLUMN_ID+" ASC";
        Integer counter=0;
        Cursor cursor=database.rawQuery("SELECT * FROM arab_indo where id=1", null);
        String[] words= word.split(" ");
        String s ="";
        if (words.length > 2) {
            for (int i=0;i<words.length;i++) {
                words[i]=words[i].replace("'","");
                s =  s+" or "+COLUMN_INDO+" like '"+words[i]+"'";
                query ="SELECT * FROM "+ TABLE_ARAB +" where ("+COLUMN_INDO+" like ?) limit 1";
                query2 ="SELECT * FROM "+ TABLE_ARAB +" where ("+COLUMN_INDO+" like ? or "+COLUMN_INDO+" like ? or "+COLUMN_INDO+" like ? or "+COLUMN_INDO+" like ?) limit 1";

                cursor = database.rawQuery(query, new String[] {words[i]});
                if (cursor.getCount()==0) {
                    cursor = database.rawQuery(query2, new String[] {words[i]+", %",words[i]+" %","% "+words[i],"% "+words[i]+" %"});
                }
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    one = new QueryData();
                    one.setArab(cursor.getString(1));
                    //one.setIndo(cursor.getString(2));
                    one.setIndo(cursor.getString(2).replaceAll("(?i)"+word,"<font color='#064B7F'><b>"+word+"</b></font>")+"\n");
                    data.add(one);
                    cursor.moveToNext();
                }
            }
        } else {
            cursor = database.rawQuery(query2, new String[] {word,word});
            cursor.moveToFirst();
            counter=cursor.getCount();
            ArrayList<String> tmp = new ArrayList<String>();
            while (!cursor.isAfterLast()) {
                one = new QueryData();
                tmp.add(cursor.getString(0));
                one.setArab(cursor.getString(1));
                //one.setIndo(cursor.getString(2));
                one.setIndo(cursor.getString(2).replaceAll("(?i)"+word,"<font color='#064B7F'><b>"+word+"</b></font>")+"\n");
                data.add(one);
                cursor.moveToNext();
            }

            cursor = database.rawQuery(query, new String[] {word+", %",word+" %","% "+word,"% "+word+" %"});
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                if(!tmp.contains(cursor.getString(0))) {
                    one = new QueryData();
                    one.setArab(cursor.getString(1));
                    //one.setIndo(cursor.getString(2));
                    one.setIndo(cursor.getString(2).replaceAll("(?i)"+word,"<font color='#064B7F'><b>"+word+"</b></font>")+"\n");
                    data.add(one);
                }
                cursor.moveToNext();
            }
            if (cursor.getCount()==0 && counter==0) {
                one = new QueryData();
                one.setIndo("maaf kata '"+word+"' tidak ditemukan dalam kamus.\nsaran: silakan gunakan padanan kata yang lain atau mengurangi/menambah imbuhan pada kata tersebut\n\nUntuk mengaktifkan keyboard Arab baca catatan di menu About. Jika masih kesulitan menggunakan aplikasi silahkan kontak kami via WA di menu 'About' aplikasi");
                data.add(one);
            }
        }
        Log.i("query", query);
        Log.i("query2", query2);


        cursor.close();
        return data;
    }

    public ArrayList<QueryData> getWordsArab_DBLama(String word){
        word = hapus_harokah(word);
        Log.i("query", word);
        char tasdid = '\u0651', alif='\u0627', alif_hamzah = '\u0623', alif_hamzah2 = '\u0625', alif_panjang='\u0622';
        StringBuffer word_sb = new StringBuffer(word);
        String s  = "";
        String s2 = "";
        String s3 = "";

        if (word.indexOf(alif)!=-1) {
            s = s+" or  "+COLUMN_ARAB_NOHAR+" like '%"+word.replace(alif,alif_hamzah)+"%' ";
            s = s+" or  "+COLUMN_ARAB_NOHAR+" like '%"+word.replace(alif,alif_hamzah2)+"%' ";
            s = s+" or  "+COLUMN_ARAB_NOHAR+" like '%"+word.replace(alif,alif_panjang)+"%' ";

            s2 = s2+" or  "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_hamzah)+" %' ";
            s2 = s2+" or  "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_hamzah2)+" %' ";
            s2 = s2+" or  "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_panjang)+" %' ";

            s3 = s3+" or "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_hamzah)+" - %' or "+COLUMN_ARAB_NOHAR+" like '% - "+word.replace(alif,alif_hamzah)+"' ";
            s3 = s3+" or "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_hamzah2)+" - %' or "+COLUMN_ARAB_NOHAR+" like '% - "+word.replace(alif,alif_hamzah2)+"' ";
            s3 = s3+" or "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_panjang)+" - %' or "+COLUMN_ARAB_NOHAR+" like '% - "+word.replace(alif,alif_panjang)+"' ";
        }

        ArrayList<QueryData> data = new ArrayList<QueryData>();
        QueryData one;
        String query="";
        String query2="";
        Integer counter=0;


        if (word.length()>2){
            query ="SELECT * FROM "+ TABLE_ARAB2 +" where ("+COLUMN_ARAB_NOHAR+" like '"+word+"%' or "+COLUMN_ARAB_NOHAR+" like '%والـ"+word+"%' or "+COLUMN_ARAB_NOHAR+" like '%ال"+word+"%' or "+COLUMN_ARAB_NOHAR+" like '%"+word+"' or "+COLUMN_ARAB_NOHAR+" like '% "+word+" %' "+s+") ORDER BY "+COLUMN_ID+" ASC";
            query2 ="SELECT * FROM "+ TABLE_ARAB2 +" where ("+COLUMN_ARAB_NOHAR+" like '"+word+"' or "+COLUMN_ARAB_NOHAR+" like '"+word+" - %' or "+COLUMN_ARAB_NOHAR+" like '% - "+word+"%' "+s3+") ORDER BY "+COLUMN_ID+" ASC";
        } else {
            query ="SELECT * FROM "+ TABLE_ARAB2 +" where "+COLUMN_ARAB_NOHAR+" like '"+word+" %' or "+COLUMN_ARAB_NOHAR+" like '% "+word+" %' "+s2+"  ORDER BY "+COLUMN_ARAB_NOHAR+" ASC";
            query2 ="SELECT * FROM "+ TABLE_ARAB2 +" where "+COLUMN_ARAB_NOHAR+" like '"+word+"'  ORDER BY "+COLUMN_ID+" ASC";
        }


        Cursor cursor=database.rawQuery("SELECT * FROM arab_indo where id=1", null);

        String[] words= word.split(" ");
        if (words.length > 1) {
            for (int i=0;i<words.length;i++) {
                s = "("+COLUMN_ARAB_NOHAR+" like '"+words[i]+"' or "+COLUMN_ARAB_NOHAR+" like 'ال"+words[i]+"' or "+COLUMN_ARAB_NOHAR+" like '"+words[i]+" - %' or "+COLUMN_ARAB_NOHAR+" like '% - "+words[i]+"')";

                query ="SELECT * FROM "+ TABLE_ARAB2 +" where "+s+" ORDER BY LENGTH("+COLUMN_ARAB_NOHAR+") ASC LIMIT 1";

                cursor = database.rawQuery(query, null);
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    one = new QueryData();
                    one.setArab(cursor.getString(2));
                    one.setIndo(cursor.getString(1));
                    data.add(one);
                    cursor.moveToNext();
                }
                Log.i("query", query);
            }
        } else {
            cursor = database.rawQuery(query2, null);
            cursor.moveToFirst();
            counter=cursor.getCount();
            ArrayList<String> tmp = new ArrayList<String>();
            while (!cursor.isAfterLast()) {
                one = new QueryData();
                tmp.add(cursor.getString(0));
                String tampung[] = (cursor.getString(5)).split(" ");
                String arabic[]  = (cursor.getString(1)).split(" ");
                Integer i;

                one.setIndo(cursor.getString(1));
                if (tampung.length <= arabic.length){
                    for (i=0;i<tampung.length;i++) {
                        if (tampung[i].contains(word)){
                            arabic[i]="<font color='#064B7F'><b>"+arabic[i]+"</b></font>";
                        }
                    }
                    String arabic_hasil="";
                    for(i=0;i<arabic.length;i++) {
                        arabic_hasil = arabic_hasil +" "+ arabic[i];
                    }

                    one.setIndo(arabic_hasil);
                }
                one.setArab(cursor.getString(2));
                //one.setIndo(cursor.getString(1));
                data.add(one);
                cursor.moveToNext();
            }
            if (word.length()>2){
                cursor = database.rawQuery(query, null);
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    if(!tmp.contains(cursor.getString(0))) {
                        one = new QueryData();
                        String tampung[] = (cursor.getString(5)).split(" ");
                        String arabic[]  = (cursor.getString(1)).split(" ");
                        Integer i;

                        one.setIndo(cursor.getString(1));
                        if (tampung.length <= arabic.length){
                            for (i=0;i<tampung.length;i++) {
                                if (tampung[i].contains(word)){
                                    arabic[i]="<font color='#064B7F'><b>"+arabic[i]+"</b></font>";
                                }
                            }
                            String arabic_hasil="";
                            for(i=0;i<arabic.length;i++) {
                                arabic_hasil = arabic_hasil +" "+ arabic[i];
                            }

                            one.setIndo(arabic_hasil);
                        }
                        one.setArab(cursor.getString(2));
                        //one.setIndo(cursor.getString(1));
                        data.add(one);
                    }
                    cursor.moveToNext();
                }
            }

            if (cursor.getCount()==0 && counter==0) {
                one = new QueryData();
                one.setArab("maaf kata '"+word+"' tidak ditemukan dalam kamus.\nsaran: silakan gunakan padanan kata yang lain.");
                data.add(one);
            }
        }

        Log.i("query", query);
        Log.i("query2", query2);

        cursor.close();
        return data;
    }
    public ArrayList<QueryData> getWordsIndo_DBLama(String word) {
        ArrayList<QueryData> data = new ArrayList<QueryData>();
        QueryData one;
        //Cursor cursor = database.rawQuery("SELECT * FROM "+TABLE_ARAB+" where "+COLUMN_INDO+" like '%"+word+"%' ORDER BY  LENGTH("+COLUMN_INDO+") ASC");
        //word="ka'bah";
        String query ="SELECT * FROM "+ TABLE_ARAB2 +" where ("+COLUMN_INDO+" like ? or "+COLUMN_INDO+" like ? or "+COLUMN_INDO+" like ? or "+COLUMN_INDO+" like ?) ORDER BY "+COLUMN_ID+" ASC";
        String query2 ="SELECT * FROM "+ TABLE_ARAB2 +" where ("+COLUMN_INDO+" like ? or "+COLUMN_INDO+" like ? or "+COLUMN_INDO+" like ? or "+COLUMN_INDO+" like ?) ORDER BY "+COLUMN_ID+" ASC";
        Integer counter=0;
        Cursor cursor=database.rawQuery("SELECT * FROM arab_indo where id=1", null);
        String[] words= word.split(" ");
        String s ="";
        if (words.length > 2) {
            for (int i=0;i<words.length;i++) {
                words[i]=words[i].replace("'","");
                s =  s+" or "+COLUMN_INDO+" like '"+words[i]+"'";
                query ="SELECT * FROM "+ TABLE_ARAB2 +" where ("+COLUMN_INDO+" like ?) limit 1";
                query2 ="SELECT * FROM "+ TABLE_ARAB2 +" where ("+COLUMN_INDO+" like ? or "+COLUMN_INDO+" like ? or "+COLUMN_INDO+" like ? or "+COLUMN_INDO+" like ?) limit 1";

                cursor = database.rawQuery(query, new String[] {words[i]});
                if (cursor.getCount()==0) {
                    cursor = database.rawQuery(query2, new String[] {words[i]+", %",words[i]+" %","% "+words[i],"% "+words[i]+" %"});
                }
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    one = new QueryData();
                    one.setArab(cursor.getString(1));
                    //one.setIndo(cursor.getString(2));
                    one.setIndo(cursor.getString(2).replaceAll("(?i)"+word,"<font color='#064B7F'><b>"+word+"</b></font>")+"\n");
                    data.add(one);
                    cursor.moveToNext();
                }
            }
        } else {
            cursor = database.rawQuery(query2, new String[] {word,word});
            cursor.moveToFirst();
            counter=cursor.getCount();
            ArrayList<String> tmp = new ArrayList<String>();
            while (!cursor.isAfterLast()) {
                one = new QueryData();
                tmp.add(cursor.getString(0));
                one.setArab(cursor.getString(1));
                //one.setIndo(cursor.getString(2));
                one.setIndo(cursor.getString(2).replaceAll("(?i)"+word,"<font color='#064B7F'><b>"+word+"</b></font>")+"\n");
                data.add(one);
                cursor.moveToNext();
            }

            cursor = database.rawQuery(query, new String[] {word+", %",word+" %","% "+word,"% "+word+" %"});
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                if(!tmp.contains(cursor.getString(0))) {
                    one = new QueryData();
                    one.setArab(cursor.getString(1));
                    //one.setIndo(cursor.getString(2));
                    one.setIndo(cursor.getString(2).replaceAll("(?i)"+word,"<font color='#064B7F'><b>"+word+"</b></font>")+"\n");
                    data.add(one);
                }
                cursor.moveToNext();
            }
            if (cursor.getCount()==0 && counter==0) {
                one = new QueryData();
                one.setIndo("maaf kata '"+word+"' tidak ditemukan dalam kamus.\nsaran: silakan gunakan padanan kata yang lain atau mengurangi/menambah imbuhan pada kata tersebut\n\nUntuk mengaktifkan keyboard Arab baca catatan di menu About. Jika masih kesulitan menggunakan aplikasi silahkan kontak kami via WA di menu 'About' aplikasi");
                data.add(one);
            }
        }
        Log.i("query", query);
        Log.i("query2", query2);


        cursor.close();
        return data;
    }

    public ArrayList<QueryData> getWordsArab(String word){
        word = hapus_harokah(word);
        Log.i("query", word);
        char tasdid = '\u0651', alif='\u0627', alif_hamzah = '\u0623', alif_hamzah2 = '\u0625', alif_panjang='\u0622';
        StringBuffer word_sb = new StringBuffer(word);
        String s  = "";
        String s2 = "";
        String s3 = "";

        if (word.indexOf(alif)!=-1) {
            s = s+" or  "+COLUMN_ARAB_NOHAR+" like '%"+word.replace(alif,alif_hamzah)+"%' ";
            s = s+" or  "+COLUMN_ARAB_NOHAR+" like '%"+word.replace(alif,alif_hamzah2)+"%' ";
            s = s+" or  "+COLUMN_ARAB_NOHAR+" like '%"+word.replace(alif,alif_panjang)+"%' ";

            s2 = s2+" or  "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_hamzah)+" %' ";
            s2 = s2+" or  "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_hamzah2)+" %' ";
            s2 = s2+" or  "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_panjang)+" %' ";

            s3 = s3+" or "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_hamzah)+" - %' or "+COLUMN_ARAB_NOHAR+" like '% - "+word.replace(alif,alif_hamzah)+"' ";
            s3 = s3+" or "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_hamzah2)+" - %' or "+COLUMN_ARAB_NOHAR+" like '% - "+word.replace(alif,alif_hamzah2)+"' ";
            s3 = s3+" or "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_panjang)+" - %' or "+COLUMN_ARAB_NOHAR+" like '% - "+word.replace(alif,alif_panjang)+"' ";
        }

        ArrayList<QueryData> data = new ArrayList<QueryData>();
        QueryData one;
        String query="";
        String query2="";
        String query3="";
        String query4="";
        Integer counter=0;


        if (word.length()>2){
            query ="SELECT * FROM "+TABLE_ARAB+" where ("+COLUMN_ARAB_NOHAR+" like '"+word+"%' or "+COLUMN_ARAB_NOHAR+" like '%والـ"+word+"%' or "+COLUMN_ARAB_NOHAR+" like '%ال"+word+"%' or "+COLUMN_ARAB_NOHAR+" like '%"+word+"' or "+COLUMN_ARAB_NOHAR+" like '% "+word+" %' "+s+") ORDER BY "+COLUMN_ID+" ASC";
            query2 ="SELECT * FROM "+TABLE_ARAB+" where ("+COLUMN_ARAB_NOHAR+" like '"+word+"' or "+COLUMN_ARAB_NOHAR+" like '"+word+" - %' or "+COLUMN_ARAB_NOHAR+" like '% - "+word+"%' "+s3+") ORDER BY "+COLUMN_ID+" ASC";
            query3 ="SELECT * FROM "+TABLE_ARAB+" where ("+COLUMN_ARAB_NOHAR+" like '"+word+"%' or "+COLUMN_ARAB_NOHAR+" like '%والـ"+word+"%' or "+COLUMN_ARAB_NOHAR+" like '%ال"+word+"%' or "+COLUMN_ARAB_NOHAR+" like '%"+word+"' or "+COLUMN_ARAB_NOHAR+" like '% "+word+" %' "+s+") ORDER BY "+COLUMN_ID+" ASC";
            query4 ="SELECT * FROM "+TABLE_ARAB+" where ("+COLUMN_ARAB_NOHAR+" like '"+word+"' or "+COLUMN_ARAB_NOHAR+" like '"+word+" - %' or "+COLUMN_ARAB_NOHAR+" like '% - "+word+"%' "+s3+") ORDER BY "+COLUMN_ID+" ASC";
        } else {
            query ="SELECT * FROM "+TABLE_ARAB+" where "+COLUMN_ARAB_NOHAR+" like '"+word+" %' or "+COLUMN_ARAB_NOHAR+" like '% "+word+" %' "+s2+"  ORDER BY "+COLUMN_ARAB_NOHAR+" ASC";
            query2 ="SELECT * FROM "+TABLE_ARAB+" where "+COLUMN_ARAB_NOHAR+" like '"+word+"'  ORDER BY "+COLUMN_ID+" ASC";
            query4 ="SELECT * FROM "+TABLE_ARAB+" where "+COLUMN_ARAB_NOHAR+" like '"+word+" %' or "+COLUMN_ARAB_NOHAR+" like '% "+word+" %' "+s2+"  ORDER BY "+COLUMN_ARAB_NOHAR+" ASC";

        }


        Cursor cursor=database.rawQuery("SELECT * FROM arab_indo where id=1", null);

        String[] words= word.split(" ");
        if (words.length > 1) {
            for (int i=0;i<words.length;i++) {
                s = "("+COLUMN_ARAB_NOHAR+" like '"+words[i]+"' or "+COLUMN_ARAB_NOHAR+" like 'ال"+words[i]+"' or "+COLUMN_ARAB_NOHAR+" like '"+words[i]+" - %' or "+COLUMN_ARAB_NOHAR+" like '% - "+words[i]+"')";
                s2 = "("+COLUMN_ARAB_NOHAR+" like '"+words[i]+" %' or "+COLUMN_ARAB_NOHAR+" like '% "+words[i]+"')";

                query ="SELECT * FROM "+TABLE_ARAB+" where "+s+" ORDER BY LENGTH("+COLUMN_ARAB_NOHAR+") ASC LIMIT 1";
                query2 ="SELECT * FROM "+TABLE_ARAB+" where "+s+" ORDER BY LENGTH("+COLUMN_ARAB_NOHAR+") ASC LIMIT 1";
                query3 ="SELECT * FROM "+TABLE_ARAB+" where "+s2+" ORDER BY LENGTH("+COLUMN_ARAB_NOHAR+") ASC LIMIT 1";

                cursor = database.rawQuery(query, null);
                if (cursor.getCount()==0){
                    cursor = database.rawQuery(query2, null);
                } else if (cursor.getCount()==0){
                    cursor = database.rawQuery(query3, null);
                }
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    one = new QueryData();
                    one.setArab(cursor.getString(2));
                    one.setIndo(cursor.getString(1));
                    data.add(one);
                    cursor.moveToNext();
                }
                Log.i("query", query);
                Log.i("query2", query2);
            }
        } else {
            cursor = database.rawQuery(query2, null);
            if (cursor.getCount()==0) {
                cursor = database.rawQuery(query4, null);
                Log.i("query4", query4);
            }
            cursor.moveToFirst();
            counter=cursor.getCount();
            ArrayList<String> tmp = new ArrayList<String>();
            while (!cursor.isAfterLast()) {
                one = new QueryData();
                tmp.add(cursor.getString(0));
                one.setArab(cursor.getString(2));
                one.setIndo(cursor.getString(1));
                data.add(one);
                cursor.moveToNext();
            }
            if (word.length()>2){
                cursor = database.rawQuery(query, null);
                if (cursor.getCount()==0) {
                    cursor = database.rawQuery(query3, null);
                    Log.i("query3", query3);
                }
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    if(!tmp.contains(cursor.getString(0))) {
                        one = new QueryData();
                        one.setArab(cursor.getString(2));
                        one.setIndo(cursor.getString(1));
                        data.add(one);
                    }
                    cursor.moveToNext();
                }
            }

            if (cursor.getCount()==0 && counter==0) {
                one = new QueryData();
                one.setArab("maaf kata '"+word+"' tidak ditemukan dalam kamus.\nsaran: silakan gunakan padanan kata yang lain");
                data.add(one);
            }
        }

        Log.i("query", query);
        Log.i("query2", query2);
//        Cursor cursor = database.rawQuery(query, null);
//        //Cursor cursor2 = database.rawQuery(query2, new String[] {word, "%"+word, word+"%", "% "+word+" %"});
//        Cursor cursor2 = database.rawQuery(query2, null);
//        cursor.moveToFirst();
//        cursor2.moveToFirst();


        cursor.close();
        return data;
    }
    public ArrayList<QueryData> getWordsArab_lisanularob(String word){
        word = hapus_harokah(word);
        char tasdid = '\u0651', alif='\u0627', alif_hamzah = '\u0623', alif_hamzah2 = '\u0625', alif_panjang='\u0622';
        StringBuffer word_sb = new StringBuffer(word);
        String s = "";

        if (word.indexOf(alif)!=-1) {
            s = s+" or  "+ARABIC_NOHAROKAH+" like '"+word.replace(alif,alif_hamzah)+"%' ";
            s = s+" or  "+ARABIC_NOHAROKAH+" like '"+word.replace(alif,alif_hamzah2)+"%' ";
            s = s+" or  "+ARABIC_NOHAROKAH+" like '"+word.replace(alif,alif_panjang)+"%' ";

        }
        ArrayList<QueryData> data = new ArrayList<QueryData>();
        QueryData one;
        //String query ="SELECT * FROM "+TABLE_ARAB_ARAB+" where "+ARABIC_NOHAROKAH+" like '"+word+"%' or "+ARABIC_NOHAROKAH+" like '"+word+" %' or "+ARABIC_ROOT +" like '"+word+"' "+s+" ORDER BY  LENGTH("+ARABIC_NOHAROKAH+") ASC";
        String query ="SELECT * FROM "+TABLE_ARAB_ARAB+" where "+ARABIC_NOHAROKAH+" like '%"+word+"%'  or "+ARABIC_ROOT +" like '%"+word+"%s' "+s+" ORDER BY  LENGTH("+ARABIC_NOHAROKAH+") ASC";
        Log.i("query", query);
        Cursor cursor = database.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            one = new QueryData();
            one.setArab(cursor.getString(1));
            one.setIndo(cursor.getString(3));
            data.add(one);
            cursor.moveToNext();
        }
        if (cursor.getCount()==0) {
            Log.i("query", query);
            one = new QueryData();
            one.setIndo("maaf kata '"+word+"' tidak ditemukan dalam kamus.\nsaran: silakan gunakan padanan kata yang lain");
            data.add(one);
        }
        cursor.close();
        return data;
    }

    public ArrayList<QueryData> getWordsArab_lisanularab(String word){
        word = hapus_harokah(word);
        char tasdid = '\u0651', alif='\u0627', alif_hamzah = '\u0623', alif_hamzah2 = '\u0625', alif_panjang='\u0622';
        StringBuffer word_sb = new StringBuffer(word);
        String s = "";

        if (word.indexOf(alif)!=-1) {
            s = s+" or  "+ARABIC_NOHAROKAH+" like '%"+word.replace(alif,alif_hamzah)+"%' ";
            s = s+" or  "+ARABIC_NOHAROKAH+" like '%"+word.replace(alif,alif_hamzah2)+"%' ";
            s = s+" or  "+ARABIC_NOHAROKAH+" like '%"+word.replace(alif,alif_panjang)+"%' ";

        }
        ArrayList<QueryData> data = new ArrayList<QueryData>();
        QueryData one;
        String query ="SELECT * FROM "+TABLE_ARAB_ARAB2+" where "+ARABIC_NOHAROKAH+" like '%"+word+"%' "+s+" ORDER BY  LENGTH("+ARABIC_NOHAROKAH+") ASC";
        Log.i("query", query);
        Cursor cursor = database.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            one = new QueryData();
            one.setArab(cursor.getString(2));
            one.setIndo(cursor.getString(1));
            data.add(one);
            cursor.moveToNext();
        }
        if (cursor.getCount()==0) {
            Log.i("query", query);
            one = new QueryData();
            one.setIndo("maaf kata '"+word+"' tidak ditemukan dalam kamus.\nsaran: silakan gunakan padanan kata yang lain");
            data.add(one);
        }
        cursor.close();
//        cursor2.close();
        return data;
    }


    public ArrayList<QueryData> getWordsArab_Quran(String word){
        word = hapus_harokah(word);
        Log.i("query", word);
        char tasdid = '\u0651', alif='\u0627', alif_hamzah = '\u0623', alif_hamzah2 = '\u0625', alif_panjang='\u0622';
        StringBuffer word_sb = new StringBuffer(word);
        String s  = "";
        String s2 = "";
        String s3 = "";

        if (word.indexOf(alif)!=-1) {
            s = s+" or  "+COLUMN_ARAB_NOHAR+" like '%"+word.replace(alif,alif_hamzah)+"%' ";
            s = s+" or  "+COLUMN_ARAB_NOHAR+" like '%"+word.replace(alif,alif_hamzah2)+"%' ";
            s = s+" or  "+COLUMN_ARAB_NOHAR+" like '%"+word.replace(alif,alif_panjang)+"%' ";

            s2 = s2+" or  "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_hamzah)+" %' ";
            s2 = s2+" or  "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_hamzah2)+" %' ";
            s2 = s2+" or  "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_panjang)+" %' ";

            s3 = s3+" or "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_hamzah)+" - %' or "+COLUMN_ARAB_NOHAR+" like '% - "+word.replace(alif,alif_hamzah)+"' ";
            s3 = s3+" or "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_hamzah2)+" - %' or "+COLUMN_ARAB_NOHAR+" like '% - "+word.replace(alif,alif_hamzah2)+"' ";
            s3 = s3+" or "+COLUMN_ARAB_NOHAR+" like '"+word.replace(alif,alif_panjang)+" - %' or "+COLUMN_ARAB_NOHAR+" like '% - "+word.replace(alif,alif_panjang)+"' ";
        }

        ArrayList<QueryData> data = new ArrayList<QueryData>();
        QueryData one;
        String query="";
        String query2="";
        Integer counter=0;
        Cursor cursor;

        query = "SELECT * FROM "+TABLE_ARAB3+" where "+COLUMN_ARAB_NOHAR+" like '%"+word+"%' "+s+"";

        /*if (word.length()>2){
            //query ="SELECT * FROM "+TABLE_ARAB3+" where ("+COLUMN_ARAB_NOHAR+" like '"+word+"%' or "+COLUMN_ARAB_NOHAR+" like '%والـ"+word+"%' or "+COLUMN_ARAB_NOHAR+" like '%ال"+word+"%' or "+COLUMN_ARAB_NOHAR+" like '%"+word+"' or "+COLUMN_ARAB_NOHAR+" like '% "+word+" %' "+s+") ORDER BY "+COLUMN_ID+" ASC";
            //query2 ="SELECT * FROM "+TABLE_ARAB3+" where ("+COLUMN_ARAB_NOHAR+" like '"+word+"' or "+COLUMN_ARAB_NOHAR+" like '"+word+" - %' or "+COLUMN_ARAB_NOHAR+" like '% - "+word+"%' "+s3+") ORDER BY "+COLUMN_ID+" ASC";

        } else {
            query ="SELECT * FROM "+TABLE_ARAB3+" where "+COLUMN_ARAB_NOHAR+" like '"+word+" %' or "+COLUMN_ARAB_NOHAR+" like '% "+word+" %' "+s2+"  ORDER BY "+COLUMN_ARAB_NOHAR+" ASC";
            query2 ="SELECT * FROM "+TABLE_ARAB3+" where "+COLUMN_ARAB_NOHAR+" like '"+word+"'  ORDER BY "+COLUMN_ID+" ASC";
        }*/


        //Cursor cursor=database.rawQuery("SELECT * FROM arab_indo where id=1", null);

         String[] words= word.split(" "); /*
        if (words.length > 1) {
            for (int i=0;i<words.length;i++) {
                s = "("+COLUMN_ARAB_NOHAR+" like '"+words[i]+"' or "+COLUMN_ARAB_NOHAR+" like 'ال"+words[i]+"' or "+COLUMN_ARAB_NOHAR+" like '"+words[i]+" - %' or "+COLUMN_ARAB_NOHAR+" like '% - "+words[i]+"')";
                s2 = "("+COLUMN_ARAB_NOHAR+" like '"+words[i]+" %' or "+COLUMN_ARAB_NOHAR+" like '% "+words[i]+"')";

                query ="SELECT * FROM "+TABLE_ARAB3+" where "+s+" ORDER BY LENGTH("+COLUMN_ARAB_NOHAR+") ASC LIMIT 1";

                cursor = database.rawQuery(query, null);
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    one = new QueryData();
                    one.setArab(cursor.getString(4)+"\n");
                    one.setIndo(cursor.getString(3)+" ("+cursor.getString(6)+":"+cursor.getString(2)+") "+"\n");
                    data.add(one);
                    cursor.moveToNext();
                }
                Log.i("query", query);
            }
        } else {
            cursor = database.rawQuery(query2, null);
            cursor.moveToFirst();
            counter=cursor.getCount();
            ArrayList<String> tmp = new ArrayList<String>();
            while (!cursor.isAfterLast()) {
                one = new QueryData();
                tmp.add(cursor.getString(0));
                one.setArab("\n"+cursor.getString(4)+"\n");
                one.setIndo(cursor.getString(3)+" ("+cursor.getString(6)+":"+cursor.getString(2)+") "+"\n");
                data.add(one);
                cursor.moveToNext();
            }*/
            //if (word.length()>2){
                cursor = database.rawQuery(query, null);
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                   // if(!tmp.contains(cursor.getString(0))) {
                        one = new QueryData();
                        String tampung[] = (cursor.getString(5)).split(" ");
                        String arabic[]  = (cursor.getString(3)).split(" ");
                        String arabic_hasil="";
                        Integer i;
                        String quran = cursor.getString(3);
                        if (tampung.length <= arabic.length && words.length<2 ){
                            for (i=0;i<tampung.length;i++) {
                                if (tampung[i].contains(word)){
                                    arabic[i]="<font color='#C71585'><b>"+arabic[i]+"</b></font>";
                                }
                            }
                            for(i=0;i<arabic.length;i++) {
                                arabic_hasil = arabic_hasil +" "+ arabic[i];
                            }

                            quran = arabic_hasil;
                        } else if(words.length>1){
                            String kalimah[]=word.split(" ");
                            Integer start=0;
                            Boolean finish=false;
                            if (cursor.getString(5).contains(word)){
                                for (i=0;i<tampung.length;i++) {
                                    if(tampung[i].contains(kalimah[0]) && tampung[i+1].contains(kalimah[1]) && !finish){
                                        arabic[i]="<font color='#C71585'><b>"+arabic[i];
                                        start=i+kalimah.length;
                                        finish=true;
                                    }
                                }
                                arabic[start]="</b></font>"+arabic[start];
                            }
                            for(i=0;i<arabic.length;i++) {
                                arabic_hasil = arabic_hasil +" "+ arabic[i];
                            }
                            quran=arabic_hasil;

                        } /* else if(words.length > 1) {
                            Integer index;
                            String ayah=cursor.getString(3);
                            index = cursor.getString(5).indexOf(word);
                            Log.i("result:",index+" "+ayah.length()+" "+word.length()+" "+cursor.getString(5).length());
                            if (index != -1){
                                if (index==0){
                                    quran="<font color='#064B7F'><b>"+ayah.substring(0,word.length())+"</b></font>"+ayah.substring(word.length()+1,ayah.length());
                                }else {
                                    quran = ayah.substring(0, index - 1) + "<font color='#064B7F'><b>" + ayah.substring(index, index + word.length()) + "</b></font>";
                                }
                                Log.i("ayah",quran);
                            }
                        }*/
                        one.setArab("\n"+"المختصر في تفسير : \n"+cursor.getString(7)+"\n"+"\n"+cursor.getString(4)+"\n");
                        //one.setIndo(cursor.getString(3)+" ("+cursor.getString(6)+":"+cursor.getString(2)+") "+"\n");
                        one.setIndo(quran+" ("+cursor.getString(6)+":"+cursor.getString(2)+") "+"\n");
                        data.add(one);
                    //}
                    cursor.moveToNext();
                }
           // }

            if (cursor.getCount()==0) {
                one = new QueryData();
                one.setArab("maaf kata '"+word+"' tidak ditemukan dalam kamus.\nsaran: silakan gunakan padanan kata yang lain");
                data.add(one);
            }
       // }

        Log.i("query", query);
        Log.i("query2", query2);

        cursor.close();
        return data;
    }
    public ArrayList<QueryData> getWordsIndo_Quran(String word) {
        ArrayList<QueryData> data = new ArrayList<QueryData>();
        QueryData one;
        Cursor cursor;
        Integer counter=0;
        //Cursor cursor = database.rawQuery("SELECT * FROM "+TABLE_ARAB+" where "+COLUMN_INDO+" like '%"+word+"%' ORDER BY  LENGTH("+COLUMN_INDO+") ASC");
        //word="ka'bah";
        String query ="SELECT * FROM "+ TABLE_ARAB3 +" where "+COLUMN_INDO+" like ? ORDER BY "+COLUMN_ID+" ASC";
        String query2 ="SELECT * FROM "+ TABLE_ARAB3 +" where "+COLUMN_INDO+" like ? or "+COLUMN_INDO+" like ? or "+COLUMN_INDO+" like ? or "+COLUMN_INDO+" like ? ORDER BY "+COLUMN_ID+" ASC";


        cursor = database.rawQuery(query2, new String[] {word,word+" %","% "+word,"% "+word+" %"});
        counter=cursor.getCount();
        ArrayList<String> tmp = new ArrayList<String>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            one = new QueryData();
            tmp.add(cursor.getString(0));
            one.setArab("\n"+cursor.getString(3)+" ("+cursor.getString(6)+":"+cursor.getString(2)+") " + "\n\n"+"المختصر في تفسير : \n"+cursor.getString(7)+"\n");
            one.setIndo(cursor.getString(4).replaceAll("(?i)"+word,"<font color='#C71585'><b>"+word+"</b></font>")+"\n\n");
            data.add(one);
            cursor.moveToNext();
        }
        if (cursor.getCount()<1) {
            cursor = database.rawQuery(query, new String[]{"%"+word+"%"});
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                if (!tmp.contains(cursor.getString(0))) {
                    one = new QueryData();
                    one.setArab(cursor.getString(3) + " (" + cursor.getString(6) + ":" + cursor.getString(2) + ") " + "\n");
                    one.setIndo(cursor.getString(4).replaceAll("(?i)" + word, "<font color='#C71585'>" + word + "</font>") + "\n");
                    data.add(one);
                }
                cursor.moveToNext();
            }
        }
        if (cursor.getCount()==0 && counter==0) {
                one = new QueryData();
                one.setIndo("maaf kata '"+word+"' tidak ditemukan dalam kamus.\nsaran: silakan gunakan padanan kata yang lain atau mengurangi/menambah imbuhan pada kata tersebut\n\nUntuk mengaktifkan keyboard Arab baca catatan di menu About. Jika masih kesulitan menggunakan aplikasi silahkan kontak kami via WA di menu 'About' aplikasi");
                data.add(one);
            }
        Log.i("query", query);
        cursor.close();
        return data;
    }

    public static String delete_duplikat(String s) {
        return s.replaceAll("(.)(\\1)+", "$1");
    }
    public String saring_word(String word){
        word = delete_duplikat(word);
        if(word.charAt(0) =='\u0627'){                              //jika huruf awal alif

        }

        return word;
    }
    private class DatabaseOpenHelper extends SQLiteAssetHelper {
        private static final String DATABASE_NAME = "kamusarabindo12.db";
        private static final int DATABASE_VERSION = 2;

        public DatabaseOpenHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
    }
}