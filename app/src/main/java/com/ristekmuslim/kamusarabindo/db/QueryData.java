package com.ristekmuslim.kamusarabindo.db;

/**
 * Created by agung on 19/11/2015.
 */
public class QueryData {
    private String arab;
    private String indo;

    public String getArab() {
        return arab;
    }

    public void setArab(String arab) {
        this.arab = arab;
    }

    public String getIndo() {
        return indo;
    }

    public void setIndo(String indo) {
        this.indo = indo;
    }
}